using FiguresLibrary;

namespace FiguresTests
{
    public class UnitTest1
    {
        [Fact]
        public void TestCicrcle()
        {
            var expected = 1661.903;
            var circle = new Circle("Circle", 23);

            var result = circle.Square();

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestTriangle()
        {
            var expected = 109.982;
            var triangle = new Triangle("Triangle", 15,16,17);

            var result = triangle.Square();

            Assert.Equal(expected, result);
        }

        [Fact]
        public void TestRightTriangle()
        {
            var triangle = new Triangle("Triangle", 15, 16, 17);

            var result = triangle.CheckTriangleIsRight();

            Assert.False(result);
        }
    }
}