﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLibrary
{
    public class Triangle : Figure
    {
        public double SideA { get; set; }
        public double SideB { get; set; }
        public double SideC { get; set; }


        public Triangle(string name, double sideA,double sideB, double sideC) : base(name)
        {
            SideA = sideA;
            SideB = sideB;
            SideC = sideC;
        }

        public override double Square()
        {
            CheckTriangle();

            var perimeter = Perimeter();
            var semiPerimeter = perimeter / 2;

            var result = Math.Sqrt(semiPerimeter * (semiPerimeter - SideA) * (semiPerimeter - SideB) * (semiPerimeter - SideC));

            return Math.Round(result, 3);
        }

        public double Perimeter()
        {
            return SideA + SideB + SideC;
        }

        private void CheckTriangle()
        {
            if (SideA < 0 || SideB < 0 || SideC < 0)
            {
                throw new ArgumentException("Side can't be less than 0!");
            }

            if ((SideA + SideB) < SideC || (SideA + SideC) < SideB || (SideB + SideC) < SideA)
            {
                throw new ArgumentException("One of sides is greater than sum of two others");
            }
        }

        public bool CheckTriangleIsRight()
        {
            var isRight = false;

            if(Math.Pow(SideA, 2) == Math.Pow(SideB, 2) + Math.Pow(SideC, 2))
            {
                isRight = true;
            }

            if (Math.Pow(SideB, 2) == Math.Pow(SideA, 2) + Math.Pow(SideC, 2))
            {
                isRight = true;
            }

            if (Math.Pow(SideC, 2) == Math.Pow(SideA, 2) + Math.Pow(SideB, 2))
            {
                isRight = true;
            }

            return isRight;
        }
    }
}
