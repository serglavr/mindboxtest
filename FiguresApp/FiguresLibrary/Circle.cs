﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLibrary
{
    public class Circle : Figure
    {
        public double Radius { get; set; }

        public Circle(string name, double radius) : base(name)
        {
            Radius = radius;
        }

        public override double Square()
        {
            CheckRadius();

            var result = Math.PI * Math.Pow(Radius,2);
            return Math.Round(result,3);
        }

        private void CheckRadius()
        {
            if (Radius < 0)
            {
                throw new ArgumentException("Radius can't be less than 0!");
            }
        }
    }
}
