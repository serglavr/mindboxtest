﻿using FiguresLibrary;

var circle = new Circle("Circle", 23);
var triangle = new Triangle("Triangle", 15, 16, 17);

var circleSquare = circle.Square();

var triangleSquare = triangle.Square();
var isRight = triangle.CheckTriangleIsRight();

Console.WriteLine($"{circle.Name} square = {circleSquare}");
Console.WriteLine(triangle.Name + " square = " + triangleSquare);

if (isRight)
{
    Console.WriteLine("Triangle is Right");
}
else
{
    Console.WriteLine("Triangle isn't right");
}